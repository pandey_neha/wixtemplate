<style id="styles">
    .border {
        border: 1px solid red;
    }

    .menu {
        top: 10px;
        width: 99.7%;
        height: 35px;
        position: fixed;
        z-index: 1;
        background-color: green;
        color: #C0C0C0;
        position: absolute !important;
    }

    nav a {
        color: #fff;
    }

    .temp_menu {
        position: absolute;
        background-color: green;
    }

    .temp_menu {
        color: #fff;
    }

    .temp_menu-3 {
        background-color: #ffffff;
        border-radius: 5px;
    }

    .temp_menu-4 {
        border: 1px solid #808080;
        color: rgba(0, 0, 0, 0.80);
        border-radius: 5px;
        background: -moz-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(241, 241, 241, 0.97) 50%, rgba(225, 225, 225, 0.97) 51%, rgba(240, 240, 240, 0.95) 84%, rgba(246, 246, 246, 0.95) 98%);
    }

    .temp_menu-li_bor {
        border-right: 1px solid #000 ! important;
    }

    .temp_menu_li {
        text-align: center;
        list-style-type: none;
        display: inline;
    }

    .temp_menu_li_1 {
        text-align: center;
        float: left;
        list-style-type: none;

    }

    .menu_list {
        width: 99.7%;
        text-align: left;
    }

    .menu_list > li {
        width: 20%;
    }

    #items {
        height: 172px;
        width: 315px;
        padding: 0px;
    }

    ol#pagination {
        overflow: hidden;
        margin-left: 20%;
    }

    ol#pagination li {
        float: left;
        margin-right: 20px;
        list-style: none;
        cursor: pointer;
        margin: 0 0 0 .5em;
    }

    .menu-animation-div {
        border: 1px solid #c0c0c0;
        border-radius: 5px;
        float: left;
        height: 55px;
        margin-left: 10px;
        margin-top: 10px;
        padding: 10px;
        text-align: center;
        width: 120px;
    }

    ol#pagination li.current {
        color: #f00;
        font-weight: bold;
    }

    .ui-button-text {
        margin-top: -5px;
    }

    img {
        width: 100%;
    }

    .style-default {
        border: 5px solid #000;
    }

    .style-noframe {
        border: none;
    }

    .style-spacial-frame-1 {
        border-radius: 50%;
    }

    .style-spacial-frame-2 {
        border-top: 5px solid #D3EDC4;
        border-left: 5px solid #D3EDC4;
        border-right: 5px solid #D3EDC4;
        border-bottom: 25px solid #D3EDC4;
    }

    .style-mouseover-img {
        border: 5px solid #fff;
        box-shadow: 0px 0px 5px 0px #000;
    }

    .style-mouseover-default {
        border: 5px solid #000;
        box-shadow: 0px 0px 5px 0px #000;
    }

    .style-lifted-shadow-img {
        position: relative;
    }

    .style-lifted-shadow-img:before, .style-lifted-shadow-img:after {
        z-index: 15;
        position: absolute;
        content: "";
        /*  bottom: 15px;*/
        left: 800px;
        /* width:50%;*/
        top: 800px;
        max-width: 300px;
        background: #000;
        -webkit-box-shadow: 0 15px 10px #000;
        -moz-box-shadow: 0 15px 10px #000;
        box-shadow: 0 15px 10px #777;
        -webkit-transform: rotate(-3deg);
        -moz-transform: rotate(-3deg);
        -o-transform: rotate(-3deg);
        -ms-transform: rotate(-3deg);
        transform: rotate(-3deg);
    }

    .style-lifted-shadow-img:after {
        -webkit-transform: rotate(3deg);
        -moz-transform: rotate(3deg);
        -o-transform: rotate(3deg);
        -ms-transform: rotate(3deg);
        transform: rotate(3deg);
        right: 10px;
        left: auto;
    }

    #slider .bar {
        padding-top: 10px;
        width: 210px !important;
        height: 5px;
        top: 4px;
        left: 4px;
        -webkit-border-radius: 40px;
        -moz-border-radius: 40px;
        border-radius: 40px;
    }

    /* Tool Tip */
    #rangevalue {
        color: #000;
        text-align: center;
        font-family: Arial, sans-serif;
        display: block;
        padding: 3px;
        border: 1px solid black;
        width: 60px !important;
        border-radius: 5px;
    }

    .last_item {
        border-right: 0px solid #000066 !important;
    }
</style>
<?php include("common-css.php") ?>
<?php include("common-js.php") ?>

<body onload="checkEdits()">
<?php $tabPosition = 1; ?>
<div id="edit_image" style="display:none;"></div>
<div id="changeable">
    <div id="menu_list" class="menu drag-element resizable">
        <nav>
            <script type="text/javascript">
                $(document).ready(function () {
                    get_index_menu();
                });
            </script>

        </nav>
    </div>

    <div id="Home" class="hide_div current page position_relative"
         style="height:2635px;background-color:#FFFAFA;border: 2px solid red;">


        <div class="drag-element resizable"
             style="position: absolute;top: 90px; left:200px; width:989px;height:500px;">
            <!-- <img name="" id="img1" class="image" src="img/banner3.jpg" alt="" style="height: 100%"
                  width="100%">-->

            <div class="container-fluid">
                <div class="row">
                    <div id="myCarouse1" class="carousel slide" data-interval="3000" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="active item">

                                <img class="img-responsive" src="img/banner1.jpg" alt="" style="height: 100%"
                                     width="100%">


                                <!--<img src="img/banner1.jpg"
                                     class="img-responsive" alt="" height="100%" width="100%">-->
                            </div>
                            <div class="item">
                                <img class="img-responsive item" src="img/banner2.jpg" alt="" style="height: 100%"
                                     width="100%">
                            </div>
                            <!-- <div class="item">
                                  <img src="img/tennis_img1.jpg"
                                       class="img-responsive item" alt="" height="100%" width="100%">
                              </div>-->
                            <div class="item">
                                <img class="img-responsive item" src="img/banner3.jpg" alt="" style="height: 100%"
                                     width="100%">
                            </div>
                            <!-- <div class="item">
                                 <img src="img/tennis3.jpeg"
                                      class="img-responsive item" alt="" height="100%" width="100%">
                             </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="drag-element resizable"
             style="position: absolute;top: 700px; left:276px; width:140px;height:140px;">

            <img name="" id="img1" class="image" src="img/icon1.jpg" alt="" style="height: 100%;
                 width=100%;border-radius: 50%;background: #d5d5d5;padding: 6px">
        </div>


        <div class="drag-element resizable"
             style="position: absolute;top: 700px; left:635px; width:140px;height:140px;">

            <img name="" id="img2" class="image" src="img/icon2.jpg" alt="" style="height: 100%;
                 width=100%;border-radius: 50%;background: #d5d5d5;padding: 6px">
        </div>


        <div class="drag-element resizable"
             style="position: absolute;top: 700px; left:994px; width:140px;height:140px;">

            <img name="" id="img3" class="image" src="img/icon3.jpg" alt="" style="height: 100%;
                 width=100%;border-radius: 50%; background: #d5d5d5;padding: 6px">
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:900px; left:200px;min-height: 70px;width: 300px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Lorem Ipsum is simply</h2>


            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:985px; left:213px;min-height: 88px;width: 287px;">
            <div id="text30" style="width: 100%;height: 100%;" contenteditable="true">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur


            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:900px; left:562px;min-height: 70px;width: 300px;">
            <div id="text31" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Lorem Ipsum is simply</h2>


            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:985px; left:575px;min-height: 88px;width: 287px;">
            <div id="text32" style="width: 100%;height: 100%;" contenteditable="true">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur


            </div>
        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:900px; left:924px;min-height: 70px;width: 300px;">
            <div id="text33" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Lorem Ipsum is simply</h2>


            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:985px; left:937px;min-height: 88px;width: 287px;">
            <div id="text34" style="width: 100%;height: 100%;" contenteditable="true">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur


            </div>
        </div>


        <div class="hide_div current page position_relative"
             style="position: absolute;top:1215px; height:466px; width:1347px; background-color:#C0C0C0;border: 2px solid red;">

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:35px ;  height:58px;left:200px; width:360px;   ">
                <div id="text35" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>WHAT CUSTOMER SAYS</h2>


                </div>


            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:108px ;  height:40px;left:200px; width:560px;  ">
                <div id="text36" style="width: 100%;height: 100%;" contenteditable="true">
                    <h4> LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING</h4>


                </div>


            </div>


            <div class="drag-element resizable"
                 style="position: absolute;top: 157px; left:200px; width:210px;height:210px;">

                <img name="" id="img4" class="image" src="img/pic1.jpg" alt="" style="height: 100%;
                 width=100%;border-radius: 50%; background: #d5d5d5;padding: 6px">
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:157px; left:434px;min-height: 210px;width: 731px;">
                <div id="text37" style="width: 100%;height: 100%;" contenteditable="true">
                    Cadipisicing elit,Nam ornare vulputate risus,id volutpat elit porttitor non.In consequat nisi vel
                    lectus dapibus sodales.
                    Nam ornare vulputate risus, id volutpat elit porttitor non. In consequat nisi vel lectus dapibus
                    sodales.
                    Nam ornare vulputate risus, id volutpat elit porttitor non. In consequat nisi vel lectus dapibus
                    sodales.
                    Nam ornare vulputate risus, id volutpat elit porttitor non. In consequat nisi vel lectus dapibus
                    sodales.
                    Pellentesque habitant morbi tristique senectus Nam ornare vulputate risus, id volutpat elit
                    porttitor non.
                    In consequat nisi vel lectus dapibus sodales. PellentesqueNam ornare vulputate risus, id volutpat
                    elit porttitor non.
                    In consequat nisi vel lectus dapibus sodales. Pellentesque habitant morbi tristique senectus et
                    netus et malesuada fames ac
                    turpis egestas Pellentesque habitant morbi tristique senectus et netus et malesuada fames.
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Nam ornare vulputate risus,id volutpat elit porttitor non. In consequat nisi vel lectus dapibus
                    sodales.Nam


                </div>
            </div>


        </div>


        <div class="hide_div current page position_relative"
             style="position: absolute;top:1680px; height:520px; width:1347px;border: 2px solid red;">


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:100px ;  height:58px;left:200px; width:360px;   ">
                <div id="text38" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>Our Latest Projects</h2>


                </div>
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 180px; left:200px; width:166px;height:113px;">

                <img name="" id="img6" class="image" src="img/gallery1.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 180px; left:369px; width:166px;height:113px;">

                <img name="" id="img7" class="image" src="img/gallery2.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 180px; left:538px; width:166px;height:113px;">

                <img name="" id="img8" class="image" src="img/gallery3.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 180px; left:707px; width:166px;height:113px;">

                <img name="" id="img9" class="image" src="img/gallery4.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>


            <div class="drag-element resizable"
                 style="position: absolute;top: 295px; left:200px; width:166px;height:113px;">

                <img name="" id="img10" class="image" src="img/gallery4.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>


            <div class="drag-element resizable"
                 style="position: absolute;top: 295px; left:369px; width:166px;height:113px;">

                <img name="" id="img11" class="image" src="img/gallery3.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 295px; left:538px; width:166px;height:113px;">

                <img name="" id="img12" class="image" src="img/gallery5.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 295px; left:707px; width:166px;height:113px;">

                <img name="" id="img13" class="image" src="img/gallery6.jpg" alt="" style="height: 100%;
                 width=100%">
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:100px ;  height:58px;left:919px; width:360px;   ">
                <div id="text38" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>Testimonials</h2>
                </div>
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:180px ;  height:129px;left:919px; width:251px;">
                <div id="text39" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        sed tempor ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt aliqua.
                        Ut enim ad minim veniam,consectetur adipisicing elit,sed do eiusmod tempor incididunt magna
                        aliqua.
                    </p>
                </div>
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:310px ;  height:129px;left:919px; width:251px;">
                <div id="text40" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        sed tempor ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt aliqua.
                        Ut enim ad minim veniam,consectetur adipisicing elit,sed do eiusmod tempor incididunt magna
                        aliqua.
                    </p>
                </div>
            </div>

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:440px ;  height:30px;left:1043px; width:127px;">
                <div id=" text41" style="width: 100%;height: 100%;" contenteditable="true">
            <p>
                - Soarlogic India.
            </p>
        </div>
        </div>

    </div>

        <div class="hide_div current page position_relative"
             style="position: absolute;top:2199px; height:350px; width:1347px; background-color:#C0C0C0;border: 2px solid red;">


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:36px ;  height:58px;left:200px; width:226px;     ">
                <div id="text42" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>GET IN TOUCH</h2>


                </div>
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:110px ;  height:146px;left:200px; width:196px;    ">
                <div id="text43" style="width: 100%;height: 100%;" contenteditable="true">


                    500 Lorem Ipsum Dolor Sit,

                    22-56-2-9 Sit Amet, Lorem,

                    USA

                  <P>  Phone:(00) 222 666 444</P>

                   <P> Fax: (000) 000 00 00 0</P>

                   <P> Email: info(at)mycompany.com</P>

                   <P> Follow on: Facebook, Twitter</P>



                </div>
            </div>



            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:36px ;  height:58px;left:540px; width:226px;     ">
                <div id="text44" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>CATEGORIES</h2>


                </div>
            </div>



            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:110px ;  height:146px;left:540px; width:305px;    ">
                <div id="text45" style="width: 100%;height: 100%;" contenteditable="true">




                    The standard chunk of Lorem Ipsum used since
                    Duis a augue ac libero euismod viverra sitth
                    Duis a augue ac libero euismod viverra sit
                    The standard chunk of Lorem Ipsum used since
                    Duis a augue ac libero euismod viverra sit
                    The standard chunk of Lorem Ipsum used since
                    Duis a augue ac libero euismod viverra sit





                </div>
            </div>



            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:36px ;  height:58px;left:880px; width:226px;     ">
                <div id="text46" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>WHAT WE DO</h2>


                </div>
            </div>





            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:110px ;  height:146px;left:880px; width:305px; ">
                <div id="text47" style="width: 100%;height: 100%;" contenteditable="true">

                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
                    ions from the 1914 below for those by H. Rackham

                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those
                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced reproduced

                </div>
            </div>

        </div>


</div>


</div>


</body>