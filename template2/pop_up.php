<div class="menu_popup_div" style="height:270px;width: 190px;position: absolute;left:600px;top:0px; z-index: 2">
    <ul class="menu_popup_ul" style="text-align: left; margin-left: 0px ! important;">
        <li class="menu_popup_list" style="background:#0000ff;color: #fff;">
            Menu
        </li>
        <li class="menu_popup_list" id="menu-setting">
            Setting
        </li>
        <li class="menu_popup_list" id="change-menu-style">
            Change Style
        </li>
        <li class="menu_popup_list" id="add-menu-animation">
            Add Animation
        </li>
    </ul>
</div>
<div class="menu-setting-popup drag"
     style="height:450px;width: 400px;position:absolute;left:400px;top: 250px; z-index: 2">
    <div class="menu_popup_header draggable element-draggable">
        <strong class="padding_left_10" style="font-size: 14px;">Menu Setting </strong>

        <div class="menu_cross_btn"><span><strong>X</strong></span></div>
    </div>
    <div class="padding_left_10" style="margin-top:40px;">
        <input class="btn btn-primary col-md-11" id="menu-style" type="button" name="" value="Change Style">
        </div>
        <div class="padding_left_10" style="margin-top:100px;">
        <input class="btn btn-primary col-md-11" id="menu-animation" type="button" name="" value="Add Animation">
    </div>
</div>
<div class="menu-style-popup drag"
     style="height:485px;width: 370px;position: absolute;left:300px;top:250px; z-index: 2">
    <div class="menu_popup_header draggable element-draggable">
        <strong class="menu_padding_left_10">Change Style</strong>

        <div class="menu_cross_btn"><span><strong>X</strong></span></div>
    </div>
    <p class="menu_padding_left_10">
        Personalize the design of this specific element, or apply a preset style below. To customize a style, click Edit
        Style.</p>

    <p class="menu_padding_left_10"><a href="#">Learn more</a></p>

    <div class="menu_padding_left_10 margin-top ">
        <button class="menu_change-style-btn personalize" name=""><strong>Personalize this Menu</strong></button>
    </div>
    <div class="padding_left_10" style="margin-top: 5px;"><p>Or apply a preset style below</p></div>
    <div class="menu_button menu_padding_left_10 margin-top" id="text_menu" onmouseover="display_style('style-default')"
         data-style='style-default'>
        <div class="menu_change-style-btns " name=""><strong>Text Menu</strong></button>
            <div class="edit_button text_menu" name=""><strong>Edit Style</strong></div>
        </div>
    </div>
    <div class="menu_button menu_padding_left_10 margin-top" id="button_menu"
         onmouseover="display_style('style-noframe')" data-style='style-noframe'>
        <div class="menu_change-style-btns" name=""><strong>Button Menu</strong></button>
            <div class="edit_button button_menu" name=""><strong>Edit Style</strong></div>
        </div>
    </div>
    <div class="menu_button menu_padding_left_10 margin-top" id="extra_menu"
         onmouseover="display_style('style-spacial-frame-1')" data-sytle='style-spacial-frame-1'>
        <div class="menu_change-style-btns" name=""><strong>Extra Menu</strong></button>
            <div class="edit_button extra_menu" name=""><strong>Edit Style</strong></div>
        </div>
    </div>
    <div class="menu_padding_left_10 margin-top"style="margin-top: 30px;"><input type="button" name="" class="btn btn-mini btn-primary" value="Cancel"
                                                        onclick="closeMenuChangeStyle()">

        <div>
        </div>
    </div>
</div>
<div class="menu-animation-popup drag"
     style="height:460px;width: 370px;position: absolute;left:300px;top:250px; z-index: 2">
    <div class="menu_popup_header draggable element-draggable">
        <strong class="menu_padding_left_10">Advanced Style</strong>

        <div class="menu_cross_btn"><span><strong>X</strong></span></div>
    </div>
    <p class="menu_padding_left_10">
        You can personalize and make changes to color, borders, frames and corners.
    </p>

    <p class="menu_padding_left_10"><a href="#">Learn more</a></p>

    <div class="menu_padding_left_10">
        <div>
            Change Skin
        </div>
        <div class="" style="height: 250px; overflow:scroll;">
            <ul id="items" style="list-style-type: none;">
                <li style="width: 100%; top: 0px; left: 0px;">
                    <div class="menu-animation-div" onmouseover="displayMenuEffect('temp_menu ')"
                         data-effect='temp_menu'>
                        <div class=" ">
                            <img src="img/menutextonly.png" alt="" class="menu-sample">
                        </div>
                    </div>
                    <div class="menu-animation-div"
                         onmouseover="displayMenuEffect('temp_menu-2'),displayMenuLi('border_right')"
                         data-effect='temp_menu-2' data-effectLi='border_right'>
                        <div class=""><img src="img/menudivider.png" alt=""></div>
                    </div>
                    <div class="menu-animation-div"
                         onmouseover="displayMenuEffect('temp_menu-3'),displayMenuLi('border_right')"
                         data-effect='temp_menu-3' data-effectLi='border_right'>
                        <div class=""><img src="img/menusolid.png" alt=""></div>
                    </div>
                    <div class="menu-animation-div"
                         onmouseover="displayMenuEffect('temp_menu-4'),displayMenuLi('border_right')"
                         data-effect='temp_menu-4' data-effectLi='border_right'>
                        <div class=""><img src="img/menusolidshiny.png" alt=""></div>
                    </div>
                </li>
                <!-- <li style="width: 100%">
                    <div class="menu-animation-div"><img src="img/menusolidshiny.png" alt=""></div>
                    <div class="menu-animation-div"><img src="img/menusolidshiny.png" alt=""></div>
                    <div class="menu-animation-div"><img src="img/menusolidshiny.png" alt=""></div>
                    <div class="menu-animation-div"><img src="img/menusolidshiny.png" alt=""></div>
                </li>
               <li style="width: 100%">
                    <div class="menu-animation-div">page3.1</div>
                    <div class="menu-animation-div">page3.2</div>
                    <div class="menu-animation-div">page3.3</div>
                    <div class="menu-animation-div">page3.4</div>
                </li>
                <li style="width: 100%">
                    <div class="menu-animation-div">page4.1</div>
                    <div class="menu-animation-div">page4.2</div>
                    <div class="menu-animation-div">page4.3</div>
                    <div class="menu-animation-div">page2.4</div>
                </li>-->
            </ul>
            <div class="menu_color_div">
                <div class="menu_color ">
                    <span class="" style="margin-top: 5px;">Dropdown Background </span>
                    <span class="pick-a-color pick-a-color-sub-menu" data-border-color="222"></span>
                </div>
                <div class="menu_color ">
                    <span class="" style="margin-top: 5px;">separators color</span>
                    <span class="pick-a-color pick-a-color-separator-color" data-border-color="222"></span>
                </div>
                <div class="menu_color ">
                    <span class="" style="margin-top: 5px;">Text </span>
                </div>
                <div class="pick-a-color pick-a-color-menu" data-border-color="222"></div>
                <div class="menu_color ">
                    <span class="" style="margin-top: 5px;">Text mouseover </span>
                </div>
                <div class="pick-a-color pick-a-color-mouse-over" data-border-color="222"></div>
                <div class="menu_color ">
                    <span class="" style="margin-top: 5px;">Text selected </span>
                </div>
                <div class="pick-a-color pick-a-color-text-select" data-border-color="222"></div>
            </div>
        </div>
        <div class="menu-animation">
            <input type="button" name="" value="Cancel" id="cancel_menu" class="btn btn-mini btn-primary" onclick="cancelAnimation()">
            <input type="button" style="margin-left: 200px;margin-top: -2px;" name="" class="btn btn-primary" value="Ok" id="" onclick="changeMenuStyle()">
        </div>
    </div>
</div>
<div class="img-popup-div"
     style="height:290px;width: 220px;position: absolute;left:600px;top: 300px; z-index: 2;">
    <ul class="popup_ul" style="text-align: left;margin-left: 0;">
        <li class="popup_list" style="background:#0000ff;color: #fff;">
            Image
        </li>
        <li class="popup_list" id="change_image" onclick=" change_bgimage();">
            Change Image
        </li>
        <li class="popup_list"
            onclick="return launchEditor();"/>
        Edit Image
        </li>
        <li class="popup_list" id="setting">
            Setting
        </li>
        <li class="popup_list" id="change-img-style">
            Change Style
        </li>
        <li class="popup_list" id="add-img-animation">
            Add Animation
        </li>
        <li class="popup_list delete">
            Delete
        </li>
    </ul>
</div>
<div class="img-setting-popup drag"
     style="height:450px;width: 400px;position:absolute;left:400px;top: 250px; z-index: 2">
    <div class="popup_header draggable element-draggable">
        <strong class="padding_left_10">Add Animation</strong>

        <div class="cross_btn"><span><strong>X</strong></span></div>
    </div>
    <div class="padding_left_10"style="margin-top:40px;">
        <input class="btn btn-primary col-md-11" id="animation" type="button" name="" value="Add Animation">
        </div>
    <div class="padding_left_10" style="margin-top:100px;">
        <input class="btn btn-primary col-md-11" id="style" type="button" name="" value="Change Style">
    </div>
</div>
<div class="img-animation-popup drag"
     style="height:450px;width: 370px;position: absolute;left:300px;top:250px; z-index: 2">
    <div class="popup_header draggable element-draggable">
        <strong class="padding_left_10">Add Animation</strong>

        <div class="cross_btn"><span><strong>X</strong></span></div>
    </div>
    <p class="padding_left_10">
        Bring your site to life with animations. To animate this element as it loads, select from
        the list below.</p>

    <p class="padding_left_10"><a href="#">Learn more</a></p>

    <div class="scroller-div">
        <div>
            Select an Animation
        </div>
        <div class="row">
            <div class="col-md-3 img-animation-div" onmouseover="display_effect('effect-fade-in')"
                 data-effect='effect-fade-in'>
                <img src="img/fadein.png" alt="">Fade in
            </div>
            <div class="col-md-3 img-animation-div" onmouseover="display_effect('effect-float-in')"
                 data-effect='effect-float-in'>
                <img src="img/floatin.png" alt="">Float in
            </div>
            <div class="col-md-3 img-animation-div" onmouseover="display_effect('effect-expand-in')"
                 data-effect='effect-expand-in'>
                <img src="img/expandin.png" alt="">Expand in
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 img-animation-div" onmouseover="display_effect('effect-spin-in')"
                 data-effect='effect-spin-in'>
                <img src="img/spin_in.png" alt="">Spin in
            </div>
            <div class="col-md-3 img-animation-div" onmouseover="display_effect('effect-fly-in')"
                 data-effect='effect-fly-in'>
                <img src="img/fly_in.png" alt="">Fly in
            </div>
            <div class="col-md-3 img-animation-div" onmouseover="display_effect('effect-turn-in')"
                 data-effect='effect-turn-in'>
                <img src="img/turn_in.png" alt="">Turn in
            </div>
        </div>
        <ul class="items" style="list-style-type: none;">
        </ul>
    </div>
    <div><p>The cursor position will show in this div.</p></div>
    <div class="padding_left_10 cancel-ok-btn">
        <input type="button" name="" value="Cancel" class="btn btn-mini btn-primary" id="cancel_animation" onclick="cancelAnimation()">
        <input class="pull-right btn btn-mini btn-primary" type="button" name="" value="Ok" id="change_animation" onclick="addAnimation()">
    </div>
</div>
<div class="text-popup-div"
     style="height:200px;width: 150px;position: absolute;left:600px;top: 300px; z-index: 2;">
    <ul class="popup_ul" style="text-align: left;margin-left: 0;">
        <li class="popup_list" style="background:#0000ff;color: #fff;">
            Text
        </li>
        <li class="popup_list" id="change_font">
            Change Page's Font
        </li>
        <li class="popup_list" id="edit-text">
            Edit Text
        </li>
        <li class="popup_list" id="add-text-animation">
            Add Animation
        </li>
        <li class="popup_list delete">
            Delete
        </li>
    </ul>
</div>
<div class="video-popup-div"
     style="height:200px;width: 150px;position: absolute;left:600px;top: 300px; z-index: 2;">
    <ul class="popup_ul" style="text-align: left;margin-left: 0;">
        <li class="popup_list" style="background:#0000ff;color: #fff;">
            Audio/Video
        </li>
        <li class="popup_list" id="change_video">
            Change Url
        </li>
        <li class="popup_list delete">
            Delete
        </li>
    </ul>
</div>
<div class="video-url-popup drag"
     style="height:250px;width: 570px;position: absolute;left:300px;top:250px; z-index: 2">
    <div class="popup_header draggable element-draggable">
        <strong class="padding_left_10">Change Audio/Video Url</strong>

        <div class="cross_btn"><span><strong>X</strong></span></div>
    </div>
    <div class="col-md-12"><p>
            • Enter embed url for video...
            like:-//www.youtube.com/embed/OOyWXALeGhQ
        </p>

        <p>
            • Enter the complete Url for audio...
        </p>
    </div>
    <div class="col-md-12">
        <div class="col-md-8">
            <input type="text" id="new-url" size="60" placeholder="Enter Url here...." style="height: 35px;"/>
        </div>
        <div class="col-md-4">
            <input type="button" class="btn btn-primary" id="change_url" value="Change"/>
        </div>
    </div>
</div>
<div class="font-popup-div drag"
     style="height:290px;width: 230px;position: absolute;left:600px;top: 300px; z-index: 2;">
    <div class="popup_header draggable element-draggable">
        <strong class="padding_left_10">Fonts</strong>

        <div class="cross_btn"><span><strong>X</strong></span></div>
    </div>
    <div class="col-md-12">
        <input id="font-type" type="text"/>
    </div>
</div>
<script type="text/javascript">
</script>
<div class="img-style-popup drag" style="height:500px;width: 370px;position: absolute;left:300px;top:250px; z-index: 2">
    <div class="popup_header draggable element-draggable">
        <strong class="padding_left_10">Change Style</strong>

        <div class="cross_btn"><span><strong>X</strong></span></div>
    </div>
    <p class="padding_left_10">
        Personalize the design of this specific element, or apply a preset style below. To customize a style, click Edit
        Style.</p>

    <p class="padding_left_10"><a href="#">Learn more</a></p>

    <div class="padding_left_10 margin-top">
        <button class="personalize-style-btn" id="personalize-style" name=""><strong>Personalize this Image</strong>
        </button>
    </div>
    <div class="padding_left_10"style="margin-top: 5px;"><p>Or apply a preset style below</p></div>
    <div class="padding_left_10 scroller-div">
        <div class="img-style-btn-div padding_left_10 margin-top" onmouseover="display_style('style-default')"
             data-style='style-default'>
            <div class="change-img-style-btns" name=""><strong>Default</strong></div>
        </div>
        <div class="img-style-btn-div padding_left_10 margin-top" onmouseover="display_style('style-noframe')"
             data-style='style-noframe'>
            <div class="change-img-style-btns" name=""><strong>No frame</strong></div>
        </div>
        <div class="img-style-btn-div padding_left_10 margin-top" onmouseover="display_style('style-spacial-frame-1')"
             data-style='style-spacial-frame-1'>
            <div class="change-img-style-btns" name=""><strong>Special frame 1</strong></div>
        </div>
        <div class="img-style-btn-div padding_left_10 margin-top" onmouseover="display_style('style-spacial-frame-2')"
             data-style='style-spacial-frame-2'>
            <div class="change-img-style-btns" name=""><strong>Special frame 2</strong></div>
        </div>
    </div>
    <div class="padding_left_10">
        <div><span>Border Width</span></div>
        <div id="slider" style="float: left;">
            <input class="bar" type="range" value="0" min="0" max="16" onchange="rangevalue.value=value"/>
        </div>
        <div style="float: left;margin-top: 5px;">
            <input type="number" id="rangevalue" value="0" min="0" max="16">
        </div>
    </div>
    <div class="padding_left_10 cancel-ok-btn">
        <input type="button" name="" value="Cancel" class="btn btn-mini btn-primary" id="cancel-style" onclick="cancelStyle()">
        <input class="pull-right btn btn-mini btn-primary" type="button" name="" value="Ok" id="change-style" onclick="addStyle()">
    </div>
</div>
<div id='injection_site'></div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('ul#items').easyPaginate({
                step:1
            });
            $(".pick-a-color").pickAColor({
                showSpectrum:true,
                showSavedColors:true,
                saveColorsPerElement:false,
                fadeMenuToggle:true,
                showAdvanced:true,
                showHexInput:false
            });
        });
        $(".pick-a-color-sub-menu").on("change", function () {
            color_code = $(this).val();
            $('.subs').css('background', '#' + color_code);
        });
        $(".pick-a-color-separator-color").on("change", function () {
            separator_color_code = $(this).val();
            $('.border_right').css('border-color', '#' + separator_color_code);
        });
        $(".pick-a-color-menu").on("change", function () {
            text_color_code = $(this).val();
            $('.text_color').css('color', '#' + text_color_code);
        });
        $(".pick-a-color-mouse-over").on("change", function () {
            var mouse_over_color_code = $(this).val();
            $('.text_color1').hover(function () {
                $(this).css('color', '#' + mouse_over_color_code);
            });
            $('.text_color1').mouseout(function () {
                $(this).css("color", "#" + text_color_code);
            });
        });
        $(".pick-a-color-text-select").on("change", function () {
            text_select_color_code = $(this).val();
            $('.select_menu').css('color', '#' + text_select_color_code);
        });

        var featherEditor = new Aviary.Feather({
            apiKey: '04ae3b89d2444898',
            apiVersion: 2,
            tools: 'all',
            appendTo: 'injection_site',
            onSave: function (imageID, newURL) {
                var img = document.getElementById(imageID);
                img.src = newURL;
                localStorage.userEdits = $('#changeable').html();
            },
            onError: function (errorObj) {
                alert(errorObj.message);
            }
        });
        function launchEditor() {
            var img = document.getElementById(selectedItemId);
            featherEditor.launch({
                image: selectedItemId,
                url: img.src
            });
            return false;
        }

    </script>