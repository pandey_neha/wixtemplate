<div id="title-content" class="editable-text drag-element resizable"
     onclick="selectedItem(),text_editable(),edit_text();"
     style="position:absolute; top:300px; left:500px; width:250px; display: none;">
    <div id="text_title" contenteditable="true">
        <h2>This is the sample title content..</h2>
    </div>
</div>
<div id="paragraph-content" class="editable-text drag-element resizable"
     onclick="selectedItem(),text_editable(),edit_text();"
     style="position:absolute; top:300px; left:500px; width:250px; display:none;">
    <div id="text_para" contenteditable="true">
        This is the paragraph content.You can add your story here.
    </div>
</div>
<div id="image-content" style="position:absolute;top:300px; left:500px; width:350px;height: 300px; display:none;">
    <img id="add_img" onclick="img_popup(),selectedItem();" src="" alt="" style="width: 100%;height: 100%;"/>
</div>
<div id="clipArt-content" style="position:absolute; top:300px; left:500px; width:350px;height: 250px; display:none;">
    <img id="add_clipArt" onclick="img_popup(),selectedItem();" src="" style="width: 100%;height: 100%;"/>
</div>
<div id="new_page0" style="display:none; position:relative;">
    <div style="background-color:white">
    </div>
</div>
<div id="new_page1" style="display:none; position:relative;">
    <div style="background-color:white">
        <div class="editable-text drag-element" onclick="text_editable()"
             style="position: absolute; top: 50px; left: 214px;width: 416px; /*border: 1px solid #808080;*/border-radius: 10px; height: 73px;">
            <div id="page_tem1">
                <h2>I'm a title. click here to edit me</h2>
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute;top: 143px; width: 1032px; height: 75px; left: 213px;border-radius: 10px;text-align: justify;/* border: 1px solid #808080;*/">
            <div id="page_tem2">
                With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. Click here to add your own text and edit me. It’s easy.
                Just click “Edit Text” or double click me to add your own content and make changes to the font.
                Feel free to drag and drop me anywhere you like on your page.
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute;top: 235px; width: 1032px; height: 75px; left: 213px;border-radius: 10px;text-align: justify; /*border: 1px solid #808080;*/">
            <div id="page_tem3">
                With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. Click here to add your own text and edit me. It’s easy.
                Just click “Edit Text” or double click me to add your own content and make changes to the font.
                Feel free to drag and drop me anywhere you like on your page. Just click “Edit Text” or double click me
                to add your own content and make changes to the font.
                Feel free to drag and drop me anywhere you like on your page.
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 336px; width: 1038px; height: 127px; left: 211px;border-radius: 10px;text-align: justify;/* border: 1px solid #808080;*/">
            <div id="page_tem4">
                With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. Click here to add your own text and edit me. It’s easy.
                Just click “Edit Text” or double click me to add your own content and make changes to the font.
                Feel free to drag and drop me anywhere you like on your page.Just click “Edit Text” or double click me
                to add your own content and make changes to the font.
                Feel free to drag and drop me anywhere you like on your page. Feel free to drag and drop me anywhere you
                like on your page. Just click “Edit Text” or double click me to add your own content and make changes to
                the font.
                Feel free to drag and drop me anywhere you like on your page. Feel free to drag and drop me anywhere you
                like on your page.
            </div>
        </div>
    </div>
</div>
<div id="new_page2" style="display: none;">
    <div style="background-color:#ffffff!important;">
        <div class="drag-element"
             style="position: absolute; top: 65px; left: 150px; text-align: center; width: 337px; border: 1px solid #808080; height: 400px;">
            <img id="page_img8" class="image" src="img/flower8.jpg" alt="" height="100%" width="100%">
        </div>
        <div class=" editable-text drag-element" onclick="text_editable()"
             style="position: absolute; top: 65px; left: 515px; width: 418px;border-radius: 10px;/* border: 1px solid #808080;*/ height: 70px;">
            <div id="pag1" contenteditable="true">
                <h2>I'm a title. click here to edit me</h2>
            </div>
        </div>
        <div class=" editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 152px; width: 643px; height: 21px; left: 515px;border-radius: 10px; /*border: 1px solid #808080;*/">
            <div id="pag2" contenteditable="true">
                With an outstanding background as a professional player and over 15 years of teaching experience,
            </div>
        </div>
        <div class=" editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 203px; width: 766px; height: 82px; left: 516px;border-radius: 10px; /*border: 1px solid #808080;*/">
            <div id="pag3" contenteditable="true">
                With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. Click here to add your own text and edit me. It’s easy.
                Just click “Edit Text” or double click me to add your own content and make changes to the font.
                Feel free to drag and drop me anywhere you like on your page.
            </div>
        </div>
        <div class=" editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 320px; width: 766px; height: 151px; left: 516px;border-radius: 10px; /*border: 1px solid #808080;*/">
            <div id="pag4" contenteditable="true">
                With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. Click here to add your own text and edit me. It’s easy.
                Just click “Edit Text” or double click me to add your own content and make changes to the font.
                Feel free to drag and drop me anywhere you like on your page.
                I’m a great place for you to tell a story and let your users know a little more about you.
                This is a great space to write long text about your company and your services.
                You can use this space to go into a little more detail about your company.
                Talk about your team and what services you provide. Talk about your team and what services you provide.
            </div>
        </div>
    </div>
</div>
<div id="new_page3" style="display:none; position:relative;">
    <div style="background-color: #ffffff">
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 62px; left: 163px; text-align: center; width: 412px;border-radius: 10px;/* border: 1px solid #808080;*/ height: 70px;">
            <div id="page_mata5" contenteditable="true">
                <h2>I'm a title. click here to edit me</h2>
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 141px; width: 1071px; height: 120px; left: 163px;border-radius: 10px;/* border: 1px solid #808080;*/ ">
            <div id="page_mata6" contenteditable="true">
                <h4>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </h4>
            </div>
        </div>
        <div class="drag-element"
             style="position: absolute; top: 260px; left: 163px; text-align: center; width: 250px; /*border: 1px solid #808080; */height: 250px;">
            <img id="page_img9" class="image" src="img/flower13.jpg" alt="" height="100%" width="100%" ;>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 350px; width: 218px; height: 61px; left: 461px;border-radius: 10px;/* border: 1px solid #808080; "
        */>
        <div id="page_mata25" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="drag-element"
         style="position: absolute; top: 260px; left: 720px; text-align: center; width: 250px; /*border: 1px solid #808080;*/ height: 250px;">
        <img id="page_img10" class="image" src="img/flower14.jpg" alt="" height="100%" width="100%" ;>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 350px; width: 218px; height: 61px; left: 1012px;border-radius: 10px;/* border: 1px solid #808080;*/ ">
        <div id="page_mata26" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 540px; width: 515px; height: 142px; left: 161px;border-radius: 10px; /*border: 1px solid #808080;*/ ">
        <div id="page_mata27" contenteditable="true">
            With an outstanding background as a professional player and over 15 years of teaching experience,
            Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
            I'm a paragraph. Click here to add your own text and edit me. It’s easy. This is a great space to write
            long text about your company and your services.
            You can use this space to go into a little more detail about your company.
            Talk about your team and what services you provide. Talk about your team and what services you provide.
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 540px; width: 514px; height: 141px; left: 721px;border-radius: 10px;/* border: 1px solid #808080;*/">
        <div id="page_mata28" contenteditable="true">
            With an outstanding background as a professional player and over 15 years of teaching experience,
            Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
            I'm a paragraph. This is a great space to write long text about your company and your services.
            You can use this space to go into a little more detail about your company.
            Talk about your team and what services you provide. Talk about your team and what services you provide.
            Click here to add your own text and edit me.
        </div>
    </div>
    <div class="drag-element"
         style="position: absolute; top: 700px; left: 161px; text-align: center; width: 250px; border: 1px solid #808080; height: 250px;">
        <img id="page_img27" class="image" src="img/flower15.jpg" alt="" height="100%" width="100%" ;>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top:770px; width: 218px; height: 61px; left: 455px;border-radius: 10px; /*border: 1px solid #808080;*/ ">
        <div id="page_mata29" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="drag-element"
         style="position: absolute; top: 700px; left: 723px; text-align: center; width: 269px; border: 1px solid #808080; height: 250px;">
        <img id="page_img28" class="image" src="img/flower8.jpg" alt="" height="100%" width="100%" ;>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 770px; width: 218px; height: 61px; left: 1014px;border-radius: 10px; /*border: 1px solid #808080;*/">
        <div id="page_mata30" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 980px; width: 512px; height: 98px; left: 160px; border-radius: 10px;/*border: 1px solid #808080; */">
        <div id="page_mata31" contenteditable="true">
            With an outstanding background as a professional player and over 15 years of teaching experience,
            Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
            I'm a paragraph. Click here to add your own text and edit me. It’s easy. This is a great space to write
            long text about your company and your services.
            You can use this space to go into a little more detail about your company.
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 980px; width: 522px; height: 96px; left: 722px;border-radius: 10px;/* border: 1px solid #808080;*/">
        <div id="page_mata32" contenteditable="true">
            With an outstanding background as a professional player and over 15 years of teaching experience,
            Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
            I'm a paragraph. This is a great space to write long text about your company and your services.
            You can use this space to go into a little more detail about your company.
        </div>
    </div>
</div>
<div id="new_page4" style="display:none; position:relative;">
</div>
<div id="new_page5" style="display:none; position:relative;">
    <div style="background-color: #ffffff ;font-size:14px;">
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 62px; left: 150px; text-align: center; width: 412px;border-radius: 10px; height: 70px;">
            <div id="page_mata50" contenteditable="true">
                <h2>I'm a title. click here to edit me</h2>
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 141px; width: 1071px; height: 100px; left: 150px;border-radius: 10px;">
            <div id="page_mata51" contenteditable="true">
                <h4>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </h4>
            </div>
        </div>
        <div class="drag-element" style="position: absolute; top: 261px; width: 400px; height: 200px; left: 150px;">
            <img id="page_img50" class="image" src="img/flower8.jpg" alt="" height="100%" width="100%">
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 261px; width: 600px; height: 200px; left: 615px;border-radius: 10px;">
            <div id="page_mata52" contenteditable="true">
                <h2> I'm a title. click here to edit me</h2>
            </div>
            <div id="page_mata53" contenteditable="true">
                <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy.</p>
            </div>
            <div id="page_mata54" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe &
                    achieve!</p>
            </div>
        </div>
        <div class="drag-element" style="position: absolute; top: 540px; width: 400px; height: 200px; left: 150px;">
            <img id="page_img51" class="image" src="img/tennis3.jpeg" alt="" height="100%" width="100%">
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 540px;width: 600px; height: 200px; left: 615px;border-radius: 10px;">
            <div id="page_mata55" contenteditable="true">
                <h2>I'm a title. click here to edit me</h2>
            </div>
            <div id="page_mata56" contenteditable="true">
                <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </p>
            </div>
            <div id="page_mata57" contenteditable="true">
                <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe &
                    achieve! </p>

            </div>
        </div>
        <div class="drag-element" style="position: absolute; top: 820px; width: 400px; height: 200px; left: 150px;">
            <img id="page_img52" class="image" src="img/tennis1.jpeg" alt="" height="100%" width="100%">
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 820px;width: 600px; height: 200px; left: 615px;border-radius: 10px;">
            <div id="page_mata599" contenteditable="true">
                <h2>I'm a title. click here to edit me</h2>
            </div>
            <div id="page_mata60" contenteditable="true">
                <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. ></p>
            </div>
            <div id="page_mata61" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe &
                    achieve!</p>
            </div>
        </div>

    </div>
</div>
<div id="new_page6" style="display:none; position:relative;">
    <div style="background-color: #ffffff">
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 62px; left: 150px; text-align: center; width: 412px;border-radius: 10px;height: 70px;">
            <div id="page_mata62" contenteditable="true">
                <h2>I'm a title. click here to edit me</h2>
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 141px; width: 1071px; height: 100px; left: 150px;border-radius: 10px;">
            <div id="page_mata63" contenteditable="true">
                <h4>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </h4>
            </div>
        </div>
        <div class="drag-element"
             style="position: absolute; top: 261px; width: 320px; height: 250px; left: 150px;border: 1px solid #808080;">
            <img id="page_img53" class="image" src="img/flower7.jpg" alt="" height="100%" width="100%">
        </div>
        <div class="drag-element"
             style="position: absolute; top: 261px; width: 320px; height: 250px; left: 520px;border: 1px solid #808080;">
            <img id="page_img54" class="image" src="img/flower8.jpg" alt="" height="100%" width="100%">
        </div>
        <div class="drag-element"
             style="position: absolute; top: 261px; width: 320px; height: 250px; left: 890px;border: 1px solid #808080;">
            <img id="page_img55" class="image" src="img/flower9.jpg" alt="" height="100%" width="100%">
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 520px;width: 320px; height: 350px; left: 150px;border-radius: 10px;text-align: justify">
            <div id="page_mata64" contenteditable="true">
                <h2>I'm a title.</h2>
            </div>
            <div id="page_mata65" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </p>
            </div>
            <div id="page_mata66" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                </p>
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute;text-align: justify; top: 520px;width: 320px; height: 350px; left: 520px;border-radius: 10px;">
            <div id="page_mata67" contenteditable="true">
                <h2>I'm a title. </h2>
            </div>
            <div id="page_mata68" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </p>
            </div>
            <div id="page_mata69" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                </p>
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute;text-align: justify; top: 520px;width: 320px; height: 350px; left: 890px;border-radius: 10px;">
            <div id="page_mata59" contenteditable="true">
                <h2>I'm a title. </h2>
            </div>
            <div id="page_mata70" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </p>
            </div>
            <div id="page_mata71" contenteditable="true">
                <p>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                </p>
            </div>
        </div>
    </div>
</div>
<div id="new_page7" style="display:none; position:relative;">
    <div style="background-color: #ffffff">
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 62px; left: 163px; text-align: center; width: 412px;border-radius: 10px;/* border: 1px solid #808080;*/ height: 70px;">
            <div id="page_mata72" contenteditable="true">
                <h2>I'm a title. click here to edit me</h2>
            </div>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 141px; width: 1071px; height: 120px; left: 163px;border-radius: 10px;/* border: 1px solid #808080;*/ ">
            <div id="page_mata73" contenteditable="true">
                <h4>With an outstanding background as a professional player and over 15 years of teaching experience,
                    Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                    I'm a paragraph. Click here to add your own text and edit me. It’s easy. </h4>
            </div>
        </div>
        <div class="drag-element"
             style="position: absolute; top: 240px; left: 163px; text-align: center; width: 250px; /*border: 1px solid #808080; */height: 250px;">
            <img id="page_img56" class="image" src="img/color2.jpg" alt="" height="100%" width="100%" ;>
        </div>
        <div class="editable-text drag-element" onclick="text_editable();"
             style="position: absolute; top: 300px; width: 218px; height: 61px; left: 461px;border-radius: 10px;/* border: 1px solid #808080; "
        */>
        <div id="page_mata74" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="drag-element"
         style="position: absolute; top: 240px; left: 720px; text-align: center; width: 250px; /*border: 1px solid #808080;*/ height: 250px;">
        <img id="page_img57" class="image" src="img/flower6.jpg" alt="" height="100%" width="100%" ;>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 300px; width: 218px; height: 61px; left: 1012px;border-radius: 10px;/* border: 1px solid #808080;*/ ">
        <div id="page_mata75" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 520px; width: 515px; height: 142px; left: 161px;border-radius: 10px; /*border: 1px solid #808080;*/ ">
        <div id="page_mata76" contenteditable="true">
            <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. Click here to add your own text and edit me. It’s easy. This is a great space to write
                long text about your company and your services.</p>

            <p> You can use this space to go into a little more detail about your company.
                Talk about your team and what services you provide. Talk about your team and what services you
                provide. </p>
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 520px; width: 514px; height: 141px; left: 721px;border-radius: 10px;/* border: 1px solid #808080;*/">
        <div id="page_mata77" contenteditable="true">
            <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. This is a great space to write long text about your company and your services. </p>

            <p>You can use this space to go into a little more detail about your company.
                Talk about your team and what services you provide. Talk about your team and what services you provide.
                Click here to add your own text and edit me. </p>
        </div>
    </div>
    <div class="drag-element"
         style="position: absolute; top: 690px; left: 161px; text-align: center; width: 250px; border: 1px solid #808080; height: 250px;">
        <img id="page_img58" class="image" src="img/color3.jpg" alt="" height="100%" width="100%" ;>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top:770px; width: 218px; height: 61px; left: 455px;border-radius: 10px; /*border: 1px solid #808080;*/ ">
        <div id="page_mata78" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="drag-element"
         style="position: absolute; top: 690px; left: 723px; text-align: center; width: 269px; border: 1px solid #808080; height: 250px;">
        <img id="page_img59" class="image" src="img/color.jpg" alt="" height="100%" width="100%" ;>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 770px; width: 218px; height: 61px; left: 1014px;border-radius: 10px; /*border: 1px solid #808080;*/">
        <div id="page_mata79" contenteditable="true">
            <h3>click here to edit me </h3>
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 960px; width: 512px; height: 98px; left: 160px; border-radius: 10px;/*border: 1px solid #808080; */">
        <div id="page_mata80" contenteditable="true">
            <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. This is a great space to write long text about your company and your services. </p>

            <p>You can use this space to go into a little more detail about your company.
                Talk about your team and what services you provide. Talk about your team and what services you provide.
                Click here to add your own text and edit me. </p>
        </div>
    </div>
    <div class="editable-text drag-element" onclick="text_editable();"
         style="position: absolute; top: 960px; width: 522px; height: 96px; left: 722px;border-radius: 10px;/* border: 1px solid #808080;*/">
        <div id="page_mata81" contenteditable="true">
            <p> With an outstanding background as a professional player and over 15 years of teaching experience,
                Karen Jenson brings both her passion and her expertise to the court, helping you believe & achieve!
                I'm a paragraph. This is a great space to write long text about your company and your services. </p>

            <p>You can use this space to go into a little more detail about your company.
                Talk about your team and what services you provide. Talk about your team and what services you provide.
                Click here to add your own text and edit me. </p>
        </div>
    </div>
</div>

<div class="gallery_images" id="gallery_1-content" onclick="selectedItem(); "
     style="position:absolute; top:300px; left:500px; width:250px; display: none; padding: 10px;">
    <div class="col-md-12" style="height: 100%;" onclick="img_popup();">
        <?php for ($i = 1; $i <= 12; $i++) {
        ?>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#">
                <img id="gallery-img<?php echo $i ?>" onclick="img_popup();" class="image"
                     src="http://placehold.it/400x300" alt="">
            </a>
        </div>
        <?php
    }
        ?>
    </div>
</div>
<div class="sliders" id="gallery_2-content" onclick="selectedItem();"
     style="position:absolute; top:300px; left:500px; width:450px; height: 200px; display: none; padding: 10px; overflow: hidden;">
    <div class="container-fluid">
        <div class="row">
            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner">
                    <div class="active item">
                        <img id="1" onclick="img_popup();"
                             src="http://wowslider.com/sliders/demo-20/data1/images/bream_bay.jpg"
                             class="img-responsive image">
                    </div>
                    <div class="item">
                        <img id="2" onclick="img_popup();"
                             src="http://wowslider.com/sliders/demo-20/data1/images/dunes_and_harbour.jpg"
                             class="img-responsive item image">
                    </div>
                    <div class="item">
                        <img id="3" onclick="img_popup();"
                             src="http://wowslider.com/sliders/demo-20/data1/images/head_rock.jpg"
                             class="img-responsive item image">
                    </div>
                    <div class="item">
                        <img id="4" onclick="img_popup();"
                             src="http://wowslider.com/sliders/demo-20/data1/images/ligurian_sea.jpg"
                             class="img-responsive item image">
                    </div>
                    <div class="item">
                        <img id="5" onclick="img_popup();"
                             src="http://wowslider.com/sliders/demo-20/data1/images/whangarei_harbour.jpg"
                             class="img-responsive item image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="video-content" style="position:absolute; top:300px; left:500px; width:450px; display: none;">
    <div id="video" class="video" onclick="video_popup(),selectedItem();" style=" padding: 10px;">
        <iframe width="100%" height="100%"
                src="https://www.youtube.com/embed/LTlv360UC5w">
        </iframe>
    </div>
</div>
<div id="audio-content" style="position:absolute; top:300px; left:500px; width:450px; display: none;">
    <div id="audio" class="audio" onclick="video_popup(),selectedItem();"
         style=" padding: 10px; width: 100%;height: 100%;">
        <audio controls src="http://www.w3schools.com/html/horse.ogg" type="audio/mp3">
            <source src="http://www.w3schools.com/html/horse.mp3" type="audio/mpeg">
            Your browser does not support the audio element.
        </audio>
    </div>
</div>
<script type="text/javascript">
    function img_popup() {
        $('.image').bind('click', function (event) {
            $('.img-popup-div').css('left', event.pageX + 0);      // <<< use pageX and pageY
            $('.img-popup-div').css('top', event.pageY);
            $('.img-popup-div').css('display', 'block');
            selectedItemId = $(this).attr('id');
            selectedItemSrc = $(this).attr('src');
            console.log(selectedItemId);
        });
    }
</script>
