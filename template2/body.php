<style id="styles">
    .border {
        border: 1px solid red;
    }

    .menu {
        top: 10px;
        width: 99.7%;
        height: 35px;
        position: fixed;
        z-index: 1;
        background-color: green;
        color: #C0C0C0;
        position: absolute !important;
    }

    nav a {
        color: #fff;
    }

    .temp_menu {
        position: absolute;
        background-color: green;
    }

    .temp_menu {
        color: #fff;
    }

    .temp_menu-3 {
        background-color: #ffffff;
        border-radius: 5px;
    }

    .temp_menu-4 {
        border: 1px solid #808080;
        color: rgba(0, 0, 0, 0.80);
        border-radius: 5px;
        background: -moz-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(241, 241, 241, 0.97) 50%, rgba(225, 225, 225, 0.97) 51%, rgba(240, 240, 240, 0.95) 84%, rgba(246, 246, 246, 0.95) 98%);
    }

    .temp_menu-li_bor {
        border-right: 1px solid #000 ! important;
    }

    .temp_menu_li {
        text-align: center;
        list-style-type: none;
        display: inline;
    }

    .temp_menu_li_1 {
        text-align: center;
        float: left;
        list-style-type: none;

    }

    .menu_list {
        width: 99.7%;
        text-align: left;
    }

    .menu_list > li {
        width: 20%;
    }

    #items {
        height: 172px;
        width: 315px;
        padding: 0px;
    }

    ol#pagination {
        overflow: hidden;
        margin-left: 20%;
    }

    ol#pagination li {
        float: left;
        margin-right: 20px;
        list-style: none;
        cursor: pointer;
        margin: 0 0 0 .5em;
    }

    .menu-animation-div {
        border: 1px solid #c0c0c0;
        border-radius: 5px;
        float: left;
        height: 55px;
        margin-left: 10px;
        margin-top: 10px;
        padding: 10px;
        text-align: center;
        width: 120px;
    }

    ol#pagination li.current {
        color: #f00;
        font-weight: bold;
    }

    .ui-button-text {
        margin-top: -5px;
    }

    img {
        width: 100%;
    }

    .style-default {
        border: 5px solid #000;
    }

    .style-noframe {
        border: none;
    }

    .style-spacial-frame-1 {
        border-radius: 50%;
    }

    .style-spacial-frame-2 {
        border-top: 5px solid #D3EDC4;
        border-left: 5px solid #D3EDC4;
        border-right: 5px solid #D3EDC4;
        border-bottom: 25px solid #D3EDC4;
    }

    .style-mouseover-img {
        border: 5px solid #fff;
        box-shadow: 0px 0px 5px 0px #000;
    }

    .style-mouseover-default {
        border: 5px solid #000;
        box-shadow: 0px 0px 5px 0px #000;
    }

    .style-lifted-shadow-img {
        position: relative;
    }

    .style-lifted-shadow-img:before, .style-lifted-shadow-img:after {
        z-index: 15;
        position: absolute;
        content: "";
        /*  bottom: 15px;*/
        left: 800px;
        /* width:50%;*/
        top: 800px;
        max-width: 300px;
        background: #000;
        -webkit-box-shadow: 0 15px 10px #000;
        -moz-box-shadow: 0 15px 10px #000;
        box-shadow: 0 15px 10px #777;
        -webkit-transform: rotate(-3deg);
        -moz-transform: rotate(-3deg);
        -o-transform: rotate(-3deg);
        -ms-transform: rotate(-3deg);
        transform: rotate(-3deg);
    }

    .style-lifted-shadow-img:after {
        -webkit-transform: rotate(3deg);
        -moz-transform: rotate(3deg);
        -o-transform: rotate(3deg);
        -ms-transform: rotate(3deg);
        transform: rotate(3deg);
        right: 10px;
        left: auto;
    }

    #slider .bar {
        padding-top: 10px;
        width: 210px !important;
        height: 5px;
        top: 4px;
        left: 4px;
        -webkit-border-radius: 40px;
        -moz-border-radius: 40px;
        border-radius: 40px;
    }

    /* Tool Tip */
    #rangevalue {
        color: #000;
        text-align: center;
        font-family: Arial, sans-serif;
        display: block;
        padding: 3px;
        border: 1px solid black;
        width: 60px !important;
        border-radius: 5px;
    }

    .last_item {
        border-right: 0px solid #000066 !important;
    }
</style>
<?php include("common-css.php") ?>
<?php include("common-js.php") ?>
<body onload="checkEdits()">
<?php $tabPosition = 1; ?>
<div id="edit_image" style="display:none;"></div>
<div id="changeable">
    <div id="menu_list" class="menu drag-element resizable">
        <nav>
            <script type="text/javascript">
                $(document).ready(function () {
                    get_index_menu();
                });
            </script>

        </nav>
    </div>
    <div id="Home" class="hide_div current page position_relative"
         style="height:1386px;background-color:#FFFAFA;border: 2px solid red;">
        <div class="drag-element resizable"
             style="position: absolute;top: 45px; left:200px; width:989px;height:500px;">
            <img name="" id="img1" class="image" src="img/slider-img2.jpg" alt="" style="height: 100%"
                 width="100%">
        </div>
        <div class="editable-text drag-element resizable"
             style="position: absolute;top:600px; left:200px;min-height: 200px;width: 600px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h1>Welcome!</h1>

                <p>Internet Cafe is one of free website templates created by TemplateMonster.com team. This web template
                    is optimized for 1280X1024 screen resolution. It is also XHTML & CSS valid.</p>

                <p>This Internet Cafe Template goes with two packages with PSD source files and without them. PSD source
                    files are available for free for the registered
                    members of TemplateMonster.com. The basic package (without PSD source) is available for anyone
                    without registration.</p>

            </div>
            <div id="end"></div>
        </div>
        <div class="editable-text drag-element resizable"
             style="position: absolute;top:801px; left:200px;min-height: 60px;width: 600px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <h2>Here you can find all services in one place:</h2>

            </div>


            <div id="end"></div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:867px; left:174px;min-height: 250px;width: 300px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">


                <ul class="list-1">
                    <li> Broadband Internet PCs with modern LCD Flat-screens, Printers, Scanners, Webcam
                    <li>CD-RW/DVD-burner, Multi-card-reader, USB-Connectors
                    <li>Laptop/Notebook stations with LAN and/or wireless access (10mbps speed)
                    <li>Rentable Laptop/Notebook cabins. (Daily rent, locked and secured overnight)
                    <li>Stabilized UPS with world-power outlets, 220V & 110V.
                </ul>

            </div>


            <div id="end"></div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:867px; left:501px;min-height: 250px;width: 299px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">


                <ul class="list-1">
                    <li> Outdoor wireless connection available 24/7 (100m radius)
                    <li> Digital Photo Printer - connect your SD-card, SmartMedia, XD-picture Card,
                        CompactFlash,
                        MemoryStick or mobile phone and print the picture direct at our photo lab station
                        (Printpix).
                        Of course you can also “unload” your camers and burn CD...
                </ul>

            </div>


            <div id="end"></div>
        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:600px; left:900px;min-height: 70px;width: 150px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h1>Tell Your Friends About Our Cafe</h1>


            </div>
        </div>


        <div class="drag-element resizable"
             style="position: absolute;top: 640px; left:1055px; width:100px;height:115px;">
            <img name="" id="img1" class="image" src="img/page1-img1.png" alt="" style="height: 100%"
                 width="100%">
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:801px; left:900px;min-height: 60px;width: 220px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <h2>Latest News</h2>

            </div>


        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:867px; left:900px;min-height: 250px;width: 220px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <p> 21 February, 2012</p>

                <p> Duis autem vel eum iriure dolor in hendrerit tum zzril delenit augue duis dolore te feugait nulla
                    facilisi.
                    Lorem ipsum dolor sit amet, consectetuer in vulputate velit esse molestie consequat vel illum augue
                    duis dolore.</p>

            </div>


        </div>


        <div class="hide_div current page position_relative"
             style="position: absolute;top:1215px; height:170px; width:1347px; background-color:#C0C0C0;border: 2px solid red;">

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:35px ;  height:80px;left:196px; width:230px;  ;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <h1>InternetCafe</h1>


                </div>


            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:55px ;  height:80px;left:435px; width:350px; ">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p> © 2014 Interior Soarlogic.com</p>

                    <p>Phone: +1 800 559 6580 Email: info@soarlogic.com</p>


                </div>


            </div>
        </div>

    </div>


        <!--<div id="Events" class="hide_div current page position_relative"
             style="height:1204px;background-color:#FFFAFA;border: 2px solid red;">
            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:150px; left:200px;min-height: 60px;width: 400px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>Everything You Want to Know</h2>


                </div>
            </div>

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:216px; left:200px;min-height: 80px;width: 620px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p> Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                        sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                        sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                    </p>

                    <p>
                        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy.
                    </p>

                </div>
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 330px; left:200px; width:230px;height:155px;">
                <img name="" id="img1" class="image" src="img/page2-img1.jpg" alt="" style="height: 100%"
                     width="100%">
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:336px; left:431px;min-height: 80px;width: 390px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        Eirmod tempor invidunt ut labore et dolore magna.

                    </p>

                    <p>
                        At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren, no sea takimata.
                    </p>

                    <p>
                        Sanctus est Lorem ipsum dolor sit amet lorem ipsum dolor sit amet, consetetur sadipscing elitr.
                    </p>

                </div>
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:486px; left:200px;min-height: 60px;width: 220px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>Special Events</h2>


                </div>
            </div>

            <div class="drag-element resizable"
                 style="position: absolute;top: 552px; left:200px; width:300px;height:155px;">
                <img name="" id="img1" class="image" src="img/page2-img2.jpg" alt="" style="height: 100%"
                     width="100%">
            </div>


            <div class="drag-element resizable"
                 style="position: absolute;top: 552px; left:510px; width:300px;height:155px;">
                <img name="" id="img1" class="image" src="img/page2-img3.jpg" alt="" style="height: 100%"
                     width="100%">
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:725px; left:200px;min-height: 210px;width: 300px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        Stet clita kasd gubergren, no sea takimata sanctus est lorem ipsum dolor


                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                        sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                    </p>


                </div>
            </div>

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:725px; left:510px;min-height: 210px;width: 300px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren


                    </p>

                    <p>
                        No sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                        consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores.
                    </p>


                </div>
            </div>

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:195px; left:900px;min-height: 70px;width: 130px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <h2>Tell Your Friends About Our Cafe</h2>


                </div>
            </div>


            <div class="drag-element resizable"
                 style="position: absolute;top: 235px; left:1040px; width:100px;height:115px;">
                <img name="" id="img1" class="image" src="img/page1-img1.png" alt="" style="height: 100%"
                     width="100%">
            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:385px; left:900px;min-height: 60px;width: 220px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                    <h2>Latest News</h2>

                </div>


            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:452px; left:900px;min-height: 80px;width: 260px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        21 February, 2012


                    </p>

                    <p>
                        Duis autem vel eum iriure dolor in hendrerit tum zzril delenit augue
                        duis dolore velit esse molestie consequat vel illum augue duis dolore.
                    </p>


                </div>
            </div>

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:650px; left:900px;min-height: 80px;width: 260px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        13 February, 2012


                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                    </p>


                </div>
            </div>

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:848px; left:900px;min-height: 80px;width: 260px;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p>
                        21 February, 2012


                    </p>

                    <p>
                        Duis autem vel eum iriure dolor duis dolore velit esse molestie consequat vel illum augue duis
                        dolore.
                    </p>


                </div>
            </div>


            <div class="hide_div current page position_relative"
                 style="position: absolute;top:1032px; height:170px; width:1347px; background-color:#C0C0C0;border: 2px solid red;">

                <div class="editable-text drag-element resizable"
                     style="position: absolute;top:35px ;  height:80px;left:196px; width:230px;  ;">
                    <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                        <h1>InternetCafe</h1>


                    </div>


                </div>


                <div class="editable-text drag-element resizable"
                     style="position: absolute;top:55px ;  height:80px;left:435px; width:350px; ">
                    <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                        <p> © 2014 Interior Soarlogic.com</p>

                        <p>Phone: +1 800 559 6580 Email: info@soarlogic.com</p>


                    </div>


                </div>
            </div>

        </div>-->


   <!--<div id="Services" class="hide_div current page position_relative"
         style="height:1081px;background-color:#FFFAFA;border: 2px solid red;">
        <div class="editable-text drag-element resizable"
             style="position: absolute;top:150px; left:200px;min-height: 60px;width: 400px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>What We Offer</h2>


            </div>
        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:216px; left:200px;min-height: 80px;width: 620px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <p> Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>

                <p>
                    At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                    sanctus est Lorem ipsum dolor sit amet.
                    Lorem ipsum dolor sit amet,
                    consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat,
                    sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                </p>

            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:410px; left:200px;min-height: 60px;width: 220px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <h2>Special Events</h2>

            </div>


        </div>

        <div class="drag-element resizable"
             style="position: absolute;top: 480px; left:200px; width:230px;height:155px;">
            <img name="" id="img1" class="image" src="img/page3-img1.jpg" alt="" style="height: 100%"
                 width="100%">
        </div>

        <div class="drag-element resizable"
             style="position: absolute;top: 670px; left:200px; width:230px;height:155px;">
            <img name="" id="img1" class="image" src="img/page3-img2.jpg" alt="" style="height: 100%"
                 width="100%">
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:485px; left:430px;min-height: 60px;width: 410px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt

                </p>

                <p>
                    Labore et dolore magna aliquyam erat,
                    sed diam voluptua at vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                    gubergren.
                </p>

            </div>


        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:675px; left:430px;min-height: 60px;width: 410px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <p>
                    Rem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor

                </p>

                <p>
                    Jnvidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                    At vero eos et accusam et justo duo dolores et ea rebum no sea takimata sanctus est.
                </p>

            </div>


        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:150px; left:900px;min-height: 60px;width: 220px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <h2>New Services</h2>

            </div>
        </div>


        <div class="drag-element resizable"
             style="position: absolute;top: 220px; left:900px; width:230px;height:115px;">
            <img name="" id="img1" class="image" src="img/page3-img3.jpg" alt="" style="height: 100%"
                 width="100%">
        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:350px; left:900px;min-height: 50px;width: 250px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                Vel eum iriure dolor in hendrerit tumzril delenit augue duis dolore.

            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:410px; left:900px;min-height: 60px;width: 220px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <h2>Our Services</h2>

            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:485px; left:874px;min-height: 293px;width: 280px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <ul class="list-1">
                    <li>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                    <li> Invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                    <li> At vero eos et accusam et justo duo dolores et ea rebum.
                    <li> Stet clita kasd gubergren, no sea
                    <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam.
                    <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam.
                </ul>

            </div>


        </div>

        <div class="hide_div current page position_relative"
             style="position: absolute;top:910px; height:170px; width:1347px; background-color:#C0C0C0;border: 2px solid red;">

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:35px ;  height:80px;left:196px; width:230px;  ;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <h1>InternetCafe</h1>


                </div>


            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:55px ;  height:80px;left:435px; width:350px; ">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p> © 2014 Interior Soarlogic.com</p>

                    <p>Phone: +1 800 559 6580 Email: info@soarlogic.com</p>


                </div>


            </div>
        </div>


    </div>-->

    <!--<div id="Jobs" class="hide_div current page position_relative"
         style="height:1082px;background-color:#FFFAFA;border: 2px solid red;">


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:150px; left:200px;min-height: 60px;width: 400px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Perspective Development</h2>


            </div>
        </div>

        <div class="drag-element resizable"
             style="position: absolute;top: 229px; left:200px; width:230px;height:155px;">
            <img name="" id="img1" class="image" src="img/page3-img3.jpg" alt="" style="height: 100%"
                 width="100%">
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:234px; left:433px;min-height: 60px;width: 410px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt.

                </p>

                <p>
                    Dabore et dolore magna aliquyam erat, sed diam voluptua vero eos et accusam et justo duo dolores et ea rebum.
                    Stet clita kasd gubergren, no sea takimata sanctus est.
                </p>

            </div>


        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:387px; left:172px;min-height: 100px;width: 559px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <ul class="list-1">
                    <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                    <li>Tempor invidunt ut labore et dolore magna aliquyam erat
                    <li>At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren, no sea
                    <li>Takimata sanctus est Lorem ipsum dolor sit amet orem ipsum dolor sit amet
                </ul>




            </div>


        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:575px; left:200px;min-height: 60px;width: 300px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Career Opportunities</h2>


            </div>
        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:643px; left:200px;min-height: 60px;width: 308px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
              <p>
                  Lorem ipsum dolor sit amet, consetetur sadip scing elitr, sed diam nonumy
              </p>
                <p>
                    Eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                    At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                    no sea takimata sanctus est Lorem ipsum dolor.
                </p>



            </div>
        </div>



        <div class="editable-text drag-element resizable"
             style="position: absolute;top:643px; left:510px;min-height: 60px;width: 308px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <p>
                    At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren

                </p>
                <p>
                    No sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>



            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:150px; left:900px;min-height: 60px;width: 200px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Our Vacations</h2>


            </div>
        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:222px; left:876px;min-height: 89px;width: 308px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <ul class="list-1">
                    <li>Consetetur sadipscing elitr sed
                    <li>Diam nonumy eirmod tempor invidunt
                    <li>Labore et dolore magna aliquyam
                    <li> At vero eos et accusam et justo duo
                    <li> Dolores et ea rebum
                </ul>




            </div>


        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:363px; left:900px;min-height: 60px;width: 242px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Interview Process</h2>


            </div>
        </div>



        <div class="drag-element resizable"
             style="position: absolute;top: 430px; left:900px; width:266px;height:113px;">
            <img name="" id="img1" class="image" src="img/page4-img2.jpg" alt="" style="height: 100%"
                 width="100%">
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:559px; left:900px;min-height: 217px;width: 265px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod magna aliquyam erat.

                </p>
                <p>
                    At vero eos et accusam et justo dolores et ea rebum.
                </p>
                <p>
                    Stet clita kasd gubergren, no sea tamata lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam.
                </p>
                <p>
                    Nonumy eirmod tempor invidunt
                </p>


            </div>
        </div>




        <div class="hide_div current page position_relative"
             style="position: absolute;top:910px; height:170px; width:1347px; background-color:#C0C0C0;border: 2px solid red;">

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:35px ;  height:80px;left:196px; width:230px;  ;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <h1>InternetCafe</h1>


                </div>


            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:55px ;  height:80px;left:435px; width:350px; ">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p> © 2014 Interior Soarlogic.com</p>

                    <p>Phone: +1 800 559 6580 Email: info@soarlogic.com</p>


                </div>


            </div>
        </div>

    </div>
-->



   <!-- <div id="Contacts" class="hide_div current page position_relative"
         style="height:1082px;background-color:#FFFAFA;border: 2px solid red;">

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:150px; left:200px;min-height: 60px;width: 400px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Contact Form</h2>


            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:250px; left:200px;min-height: 450px;width: 400px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">



            </div>
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:195px; left:900px;min-height: 70px;width: 130px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <h2>Tell Your Friends About Our Cafe</h2>


            </div>
        </div>


        <div class="drag-element resizable"
             style="position: absolute;top: 235px; left:1040px; width:100px;height:115px;">
            <img name="" id="img1" class="image" src="img/page1-img1.png" alt="" style="height: 100%"
                 width="100%">
        </div>


        <div class="editable-text drag-element resizable"
             style="position: absolute;top:385px; left:900px;min-height: 60px;width: 220px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">

                <h2>Latest News</h2>

            </div>


        </div>

        <div class="editable-text drag-element resizable"
             style="position: absolute;top:452px; left:900px;min-height: 246px;width: 228px;">
            <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                <p>
                    24 Hour Emergency Towing


                </p>

                <p>
                    Monday - Friday: 7:30 am - 6:00

                    Saturday: 7:30 am - Noon.
                </p>
               <p>
                   Night Drop Available
                </p>
                <p>
                    Demolink.org 8901 Marmora Road, Glasgow, D04 89GR
                </p>

                <p>
                    Telephone:+1 959 552 5963;
                    FAX:+1 959 552 5963
                    E-mail:mail@soarlogic.org
                </p>

            </div>
        </div>


        <div class="hide_div current page position_relative"
             style="position: absolute;top:910px; height:170px; width:1347px; background-color:#C0C0C0;border: 2px solid red;">

            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:35px ;  height:80px;left:196px; width:230px;  ;">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <h1>InternetCafe</h1>


                </div>


            </div>


            <div class="editable-text drag-element resizable"
                 style="position: absolute;top:55px ;  height:80px;left:435px; width:350px; ">
                <div id="text29" style="width: 100%;height: 100%;" contenteditable="true">
                    <p> © 2014 Interior Soarlogic.com</p>

                    <p>Phone: +1 800 559 6580 Email: info@soarlogic.com</p>


                </div>


            </div>
        </div>

    </div>-->






</div>

</body>